package com.example.demothymeleaf.controller;

import com.example.demothymeleaf.Model.Persons;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;

@Controller
@RequestMapping("/persons")
public class Person {

    @RequestMapping("/get-text")
    @ResponseBody
    public String getText(){
        ArrayList<String> virus=new ArrayList<>();
        virus.add("rozy");
        virus.add("na");
        return virus.toString();
    }

    @RequestMapping("/get-index")
    public String getindex(ModelMap modelMap){
        String name="Hello SR3";
        String gender="Female";
        int id=10;
        Persons persons=new Persons(1,"tangho","Male");
        ArrayList <Persons> ps=new ArrayList<>();
        for(int i=1;i<=5;i++){
            ps.add(new Persons(i,"virus","male"));
        }
        modelMap.addAttribute("persons",persons);
        modelMap.addAttribute("name",name);
        modelMap.addAttribute("gender",gender);
        modelMap.addAttribute("id",id);
        modelMap.addAttribute("ps",ps);
        return "index";
    }


    @RequestMapping("/get-layout")
    public String getlayout(){
        return "layout";
    }



}
